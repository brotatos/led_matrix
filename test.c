#include "gui.h"
#include "test.h"
#include <stdio.h>
#include <stdlib.h>

int main(void) {
   char* matrix[MATRIX_LEN];
   int i, j;

   for(i = 0; i < MATRIX_LEN; i++) {
     matrix[i] = malloc(MATRIX_LEN);
     if(!matrix[i]) {
       perror("malloc failed");
     }
   }
   
   for(i =0; i < MATRIX_LEN; i++) {
     for(j = 0; j < MATRIX_LEN; j++) {
       matrix[i][j] = '!';
     }
   }
   // In here
   printf("%c\n", matrix[0][0]);
   print_matrix(matrix);

   return 0;
}

void print_matrix(char** matrix) {
   int col, row;
   puts("before");

   for (col = 0; col < MATRIX_LEN; col++) {
      for (row = 0; row < MATRIX_LEN; row++) {
         // If the column  isn't equivalent to the max length spaces, add a
         // space.  Otherwise, it will be the end of the line where a newline
         // is needed rather than a space.
         printf(row != MAX_LEN_SPACES ? "%c " : "%c\n", matrix[row][col]);
      }
   }
}

void draw_matrix() {
}
