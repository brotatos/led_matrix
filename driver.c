#define F_CPU 16000000LU

#include <stdbool.h>
#include <stdint.h>
#include <util/delay.h>
#include "base.h"
#include "gui.h"

int main(void) {
   struct pos mainPos = {TOP, 0, 0};
   int i;
   //enum DIRECTION dir = 0;
   //uchar cnt = 0;
   //uint16_t clock_div = 0;

   
   cube_init();
   while(true) {
      for(i = 0; i < LEDS_LEN; i++) {
         line(LEDS_LEN, mainPos, EAST, RED);
         mainPos.row++;
         _delay_ms(1);
      }
   
      mainPos.row = 0;

      for(i = 0; i < LEDS_LEN; i++) {
         line(LEDS_LEN, mainPos, EAST, GREEN);
         mainPos.row++;
         _delay_ms(1);
      }
      
      mainPos.row = 0;
      
      for(i = 0; i < LEDS_LEN; i++) {
         blank(mainPos, 1, LEDS_LEN);
         mainPos.row++;
         _delay_ms(1);
      }
   }
   
   while(true);
   return 0;
}
