/* IEEE Student Branch Cal Poly
 * LED Matrix Project
 * Hardware:
 *    Tannis Hodge
 *    Anlang Lu
 *    Traci Takasugi
 * Software:
 *    Jacob Hladky (base.c)
 *    Robin Choudhury (gui.c)
 */
#define F_CPU 16000000UL
#include <inttypes.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdbool.h>
#include "base.h"

volatile static enum COLOR leds[LEDS_LEN][LEDS_LEN];
uint16_t clkCnt = 0;

void single_led(struct pos pos, enum COLOR color) {
   leds[pos.col][pos.row] = color;
}

void _single_led(struct pos pos, enum COLOR color) {
   uchar mtx = pos.row / MTX_LEN * FACE_LEN  + pos.col / MTX_LEN;
   uchar col = pos.col % 8;
   uchar portcTmp;

   portcTmp = 0x00;
   if(mtx < 6) {
      portcTmp |= (1 << MTX_6_DISABLE) | (1 << MTX_MUX_ENABLE);
      portcTmp |= (mtx << 3);
   } else if (mtx == 6) {
      portcTmp &= ~(1 << MTX_6_DISABLE);
      portcTmp &= ~(1 << MTX_MUX_ENABLE);
   } else {
      portcTmp |= (1 << MTX_6_DISABLE) | (1 << MTX_MUX_ENABLE);
      portcTmp |= ((mtx - 1) << 3);
   }

   portcTmp |= (pos.row % MTX_LEN);
   
   if(col < 4) {
      if(color == OFF) {
         SPI_MasterTransmit(0x00U);
         SPI_MasterTransmit(0x00U);
      } else {
         SPI_MasterTransmit(0x00U);
         SPI_MasterTransmit((color == RED ? 0x80U : 0x40U) >> col * 2);
      }
   } else {
      if(color == OFF) {
         SPI_MasterTransmit(0x00U);
         SPI_MasterTransmit(0x00U);
      } else {
         SPI_MasterTransmit((color == RED ? 0x40U : 0x80U) >> (7 - col) * 2);
         SPI_MasterTransmit(0x00);
      }
   }
   
   PORTB |= (1 << XLAT);
   PORTB &= (0 << XLAT);
   PORTC = portcTmp;
}

void blank_led(struct pos pos) {
   leds[pos.col][pos.row] = OFF;
}

void blank(struct pos pos, uchar rows, uchar cols) {
   uchar i, j;
   uchar iMax = pos.col + cols, jMax = pos.row + rows;
   
   for(i = pos.col; i < iMax; i++) {
      for(j = pos.row; j < jMax; j++) {
         leds[i][j] = OFF;
      }
   }
}

ISR(TIMER0_COMPA_vect) {
   static struct pos isrPos = {TOP, 0, 0};

   clkCnt++;
   _single_led(isrPos, leds[isrPos.col][isrPos.row]);
   isrPos.col++;
   if(isrPos.col == LEDS_LEN) {
      isrPos.col = 0;
      isrPos.row++;
   }
   if(isrPos.row == LEDS_LEN) {
      isrPos.row = isrPos.col = 0;
   }
}

void cube_init(void) {
   uchar i, j;
   
   DDRD |= 1<<PD6; //this is our test LED
   PORTD |= 1<<PD6; //dead simple: it's always on. If it's off, there's a short
   
   DDRB |= (1 << XLAT); //set latch to be an out pin
   PORTB &= (0 << XLAT); //latch is low initially
   
   DDRC = 0xFF; //all pins from register C are out pins

   PRR &= ~(1 << PRTIM0); //enable timer0
   //TCCR0A |= (1 << WGM01); //set the timer to clear on compare
   TCCR0A |= (1 << COM0A1);
   TCCR0B |= (1 << CS00);  //set timer to have no prescaling
   TIMSK0 |= (1 << OCIE0A); //we want an interrupt on OCR0A match
   
   OCR0A = 79; //Corresponds to an interrupt every 10uS
   
   for(i = 0; i < LEDS_LEN; i++) {
      for(j = 0; j < LEDS_LEN; j++) {
         leds[i][j] = OFF;
      }
   }
   
   SPI_MasterInit(); //enable SPI
   sei(); //enable interrupts
}

void SPI_MasterInit(void) {
   //Set MOSI and SCK output, all others input
   DDRB |= (1<<DDB5) | (1<<DDB7) | (1<<DDB4); //change back to = ???

   //Enable SPI, Master, set clock rate fck/16
   SPCR = (1<<SPE) | (1<<MSTR) | (1<<SPR0);
}

void SPI_MasterTransmit(uchar cData) {
   //Start transmission 
   SPDR = cData;

   //Wait for transmission complete
   while(!(SPSR & (1<<SPIF)))
      ; 
}
