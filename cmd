#!/bin/bash

avr-gcc -mmcu=atmega644a -g -Os -c driver.c base.c gui.c 
avr-gcc -mmcu=atmega644a -o base.elf base.o driver.o gui.o
avr-objcopy -j .text -j .data -O ihex base.elf base.hex
avrdude -c usbtiny -p m644 -U flash:w:base.hex
