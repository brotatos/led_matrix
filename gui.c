#include <stdbool.h>
#include "base.h"
#include "gui.h"

// Subtracting to go up or west.
// Addition to go south or east.
void line(uchar length, struct pos pos, enum DIRECTION dir, enum COLOR color) {
   int count;

   for (count = 0; count < length; count++) {
      single_led(pos, color);
      switch (dir) {
      case NORTH:
         pos.row--;
         break;
      case EAST:
         pos.col++;
         break;
      case SOUTH:
         pos.row++;
         break;
      case WEST:
         pos.col--;
         break;
      case NORTHEAST:
         pos.col++;
         pos.row--;
         break;
      case SOUTHEAST:
         pos.col++;
         pos.row++;
         break;
      case SOUTHWEST:
         pos.col--;
         pos.row++;
         break;
      default:
      case NORTHWEST:
         pos.col--;
         pos.row--;
         break;
      }
   }
}

void square(uchar n, struct pos pos, bool filled, enum COLOR color) {
   if (!filled) {
      uchar curRow = pos.row;
      uchar curCol = pos.col;
      uchar finalRow = pos.row + n - 1;
      // Fill the top row.
      line(n, pos, EAST, color);
      // Fill the bottom row.
      pos.row = finalRow;
      line(n, pos, EAST, color);
      // Fill the edges.
      while (pos.row-- != curRow) {
         single_led(pos, color);
         pos.col += n - 1;
         single_led(pos, color);
         pos.col = curCol;
      }
   } else {
      int size_of_square = 0;
      while (size_of_square++ < n) {
         line(n, pos, EAST, color);
         pos.row++;
      }
   }
}

void circle(uchar radius, struct pos pos, enum COLOR color) {
   uchar x = radius, y = 0, radiusError = 1-x, x0 = pos.col, y0 = pos.row;
   uchar length_of_line = 0;
   struct pos line_pos = {TOP, 0, 0};

   // column = x
   // row = y <- use this to set the length of the line
   while(x >= y)
   {
      // line(uchar length, struct pos pos, enum DIRECTION dir, enum COLOR color)
      line_pos.row = y + y0;
      line_pos.col = x + x0;
      length_of_line = line_pos.row - line_pos.col;
      line(length_of_line, line_pos, EAST, color);
      //single_led(x + x0, y + y0);
      //single_led(y + x0, x + y0);
      //single_led(-x + x0, y + y0);
      //single_led(-y + x0, x + y0);
      //single_led(-x + x0, -y + y0);
      //single_led(-y + x0, -x + y0);
      //single_led(x + x0, -y + y0);
      //single_led(y + x0, -x + y0);

      y++;
      if(radiusError<0) {
         radiusError+=2*y+1;
      } else {
         x--;
         radiusError+=2*(y-x+1);
      }
   }
}

void letter(char ltr, enum SIZE size, struct pos pos, enum COLOR color) {
   uint8_t letters_small[NUM_LETTERS][LETTER_HEIGHT_S] = {
      //A
      {0b00000110,
       0b00001001,
       0b00001111,
       0b00001001,
       0b00001001
      },
      //B
      {0b00001000,
       0b00001000,
       0b00001111,
       0b00001001,
       0b00001111
      },
      //C
      {0b00001111,
       0b00001000,
       0b00001000,
       0b00001000,
       0b00001111
      },
      //D
      {0b00000001,
       0b00000001,
       0b00001111,
       0b00001001,
       0b00001111
      },
      //E
      {0b00001111,
       0b00001000,
       0b00001111,
       0b00001000,
       0b00001111
      },
      //F
      {0b00001111,
       0b00001000,
       0b00001110,
       0b00001000,
       0b00001000
      },
      //G
      {0b00001111,
       0b00001001,
       0b00001111,
       0b00000001,
       0b00001111
      },
      //H
      {0b00001000,
       0b00001000,
       0b00001111,
       0b00001001,
       0b00001001
      },
      //I
      {0b00001000,
       0b00001000,
       0b00001000,
       0b00001000,
       0b00001000
      },
      //J
      {0b00000001,
       0b00000001,
       0b00000001,
       0b00001001,
       0b00001111
      },
      //K
      {0b00001001,
       0b00001010,
       0b00001100,
       0b00001010,
       0b00001001
      },
      //L
      {0b00001000,
       0b00001000,
       0b00001000,
       0b00001000,
       0b00001111
      },
      //M
      {0b00001001,
       0b00001111,
       0b00001001,
       0b00001001,
       0b00001001
      },
      //N
      {0b00000000,
       0b00001000,
       0b00001111,
       0b00001001,
       0b00001001
      },
      //O
      {0b00001111,
       0b00001001,
       0b00000001,
       0b00001001,
       0b00001111
      },
      //P
      {0b00001111,
       0b00001001,
       0b00001111,
       0b00001000,
       0b00001000
      },
      //Q
      {0b00001111,
       0b00001001,
       0b00001111,
       0b00001000,
       0b00001100
      },
      //R
      {0b00001111,
       0b00001001,
       0b00001111,
       0b00001010,
       0b00001001
      },
      //S
      {0b00001111,
       0b00001000,
       0b00001111,
       0b00000001,
       0b00001111
      },
      //T
      {0b00000100,
       0b00000100,
       0b00001111,
       0b00000100,
       0b00000100
      },
      //U
      {0b00001001,
       0b00001001,
       0b00001001,
       0b00001001,
       0b00001111
      },
      //V
      {0b00001001,
       0b00001001,
       0b00001010,                                              
       0b00001010,
       0b00001100
      },
      //W
      {0b00001001,
       0b00001001,
       0b00001101,                                              
       0b00001011,
       0b00001111
      },
      //X
      {0b00001001,
       0b00000110,
       0b00000101,
       0b00001001,
       0b00001001
      },
      //Y
      {0b00001001,
       0b00001001,
       0b00000010,                                              
       0b00001010,
       0b00000100
      },
      //Z
      {0b00001111,
       0b00000010,
       0b00000100,                                              
       0b00001000,
       0b00001111
      }
   };

   uint8_t letters_med[NUM_LETTERS][LETTER_HEIGHT_M];
   uint16_t letters_large[NUM_LETTERS][LETTER_HEIGHT_L] = {
      //A
      {0b0000000000000000,
       0b0000000000000000,
       0b0000000000000000,
       0b0000000000000000,

       0b0000000000000000,
       0b0000000000000000,
       0b0000000000000000,
       0b0000000000000000,

       0b0000000000000000,
       0b0000000000000000,
       0b0000000000000000,
       0b0000000000000000,

       0b0000000000000000,
       0b0000000000000000,
       0b0000000000000000,
       0b0000000000000000,

       0b0000000000000000,
       0b0000000000000000,
       0b0000000000000000,
       0b0000000000000000,
      }
   };

   uchar sizes[3][2] = {
      {LETTER_HEIGHT_S, LETTER_WIDTH_S},
      {LETTER_HEIGHT_M, LETTER_WIDTH_M},
      {LETTER_HEIGHT_L, LETTER_WIDTH_L}
   };
   uchar i, j;
   struct pos tmpPos;

   for(i = 0; i < sizes[size][HEIGHT]; i++) {
      for(j = 0; j < sizes[size][WIDTH]; j++) {
         tmpPos.row = i + pos.col;
         tmpPos.col = j + pos.row;
         //if(size == SMALL) {
         single_led(tmpPos, ((letters_small[ltr - 'A'][i]) & (1 << (3 - j))) ? color : OFF);
            //} else if(size == MED) {
            //single_led(tmpPos, letters_med[ltr - 'A'][i][j] ? color : OFF);
            //} else {
            //single_led(tmpPos, letters_large[ltr - 'A'][i][j] ? color : OFF;
            //}
      }
   }
}
