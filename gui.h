#ifndef GUI_H
#define GUI_H
#include "base.h"
#include <stdbool.h>

#define NUM_LETTERS 36 //for now
#define LETTER_HEIGHT_S 5
#define LETTER_WIDTH_S 4

#define LETTER_HEIGHT_M 10
#define LETTER_WIDTH_M 8

#define LETTER_HEIGHT_L 20
#define LETTER_WIDTH_L 16

#define WIDTH 1
#define HEIGHT 0

/* Used to draw on 24x24 matrix.  */
/*
 * Draw a square letter of SIZE (small, medium, large) at position (col, row),
 * which will be upper left of the letter.
 */
void letter(char ltr, enum SIZE size, struct pos pos, enum COLOR color);

/*
 * Draw a square of size nxn at position pos which will be the upper left of
 * the square.
 * The boolean filled determines if the inner part of the square will be filled
 * as opposed to merely drawing the border.
 */
//void square(uchar n, struct pos pos, bool filled, enum COLOR color);

void circle(uchar radius, struct pos pos, enum COLOR color);

void line(uchar length, struct pos pos, enum DIRECTION dir, enum COLOR color);
#endif
