#ifndef TEST_H
#define TEST_H

/* Test will be used to output LED manipulations to STDOUT or possibly a
 * file.
 */
#define TEST_SYM 'o'
#define MATRIX_LEN 24
#define MAX_LEN_SPACES 23

void draw_matrix();
void print_matrix(char** matrix);

#endif
