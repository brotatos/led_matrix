CC=avr-gcc
TEST_CC=gcc
CFLAGS=-mmcu=atmega644a -g -Os -Wall -Wextra -pedantic -std=c99
TEST_CFLAGS=-g -Os -Wall -Wextra -pedantic -std=c99
LDFLAGS=-lm

all: cube program

cube: base.o
	avr-gcc -mmcu=atmega644a -o $< base.elf
	ovr-objcopy -j .text -j .data -0 ihex base.elf base.hex

program: base.hex
	avrdude -c usbtiny -p m644 -U flash:w:base.hex

test:
	$(TEST_CC) $(TEST_CFLAGS) -o test test.c

clean:
	$(RM) test *.o *.hex *.elf

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

base.c: base.h
	touch $@

.PHONY: all clean
