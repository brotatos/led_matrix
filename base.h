#ifndef BASE_H
#define BASE_H

#include <inttypes.h>

#define uchar unsigned char
#define ushort unsigned short
#define uint unsigned int

#define XLAT PB4
#define SIN PB5
#define SCLK PB7

#define ROW_MUX0 PC0
#define ROW_MUX1 PC1
#define ROW_MUX2 PC2

#define MTX_MUX0 PC3
#define MTX_MUX1 PC4
#define MTX_MUX2 PC5
#define MTX_MUX_ENABLE PC7
#define MTX_6_DISABLE PC6

#define MTX_LEN 8
#define FACE_LEN 3
#define LEDS_LEN 24

enum FACE { TOP, BOTTOM, FAR, NEAR, LEFT, RIGHT };
enum COLOR { RED, GREEN, ORANGE, OFF};
enum SIZE { SMALL, MED, LARGE };

enum DIRECTION {
   NORTH,
   EAST,
   SOUTH,
   WEST,
   NORTHEAST,
   SOUTHEAST,
   SOUTHWEST,
   NORTHWEST
};

struct pos {
   enum FACE face;
   uchar row;
   uchar col;
};

void single_led(struct pos pos, enum COLOR color);
void blank_led(struct pos pos);
void blank(struct pos, uchar rows, uchar cols);

void cube_init(void);
void SPI_MasterInit(void);
void SPI_MasterTransmit(uchar cData);

extern uint16_t clkCnt;
#endif
